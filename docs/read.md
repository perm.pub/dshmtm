Reading Successions
===================
<!-- copybreak off -->

This section provides instructions for retrieving the data for a given Document
Succession Identifier (DSI).
The shell environment variable `BASEDSI` is used in the following instructions
as the base DSI of the document succession to retrieve.
An example setting of `BASEDSI` is:

```bash
export BASEDSI=1wFGhvmv8XZfPx0O5Hya2e9AyXo
```


Basic Operations
----------------

### Convert Base DSI to Git Commit Hash

```bash
export GITHASH=$(echo ${BASEDSI}= | basenc --decode --base64url | xxd -plain)
echo $GITHASH
```

<!-- copybreak off -->
### Find GitHub Repositories for a Document Succession

To locate all GitHub repositories containing
the document succession identified by the base DSI `${BASEDSI}`,
perform the following steps after setting `GITHASH` as instructed previously:

```bash
curl --request GET \
  --header "Accept: application/vnd.github+json" \
  --header "X-GitHub-Api-Version: 2022-11-28" \
  "https://api.github.com/search/commits?q=hash:${GITHASH}" \
  | tee response.json | jq .total_count
```

```bash
export JQFILTER='.items[].repository | "gh-" + (.id | tostring) + " https://github.com/" + .full_name + ".git"'
cat response.json \
  | jq --raw-output "${JQFILTER}" \
  | tee repos.txt
```


### Initialize Git Repository to Fetch Document Successions

Given a text file `repos.txt` as generated previously,
the following commands will create
a local Git repository that is set up to fetch updates to a document succession found in GitHub repositories.

```bash
git init mycopy
cd mycopy
cat repos.txt | xargs -l1 git remote add
```

!!! warning
    This manual assumes the default branches of the fetched repositories are all named 'main'.

### Fetch Updates from Git Repository

```bash
git fetch --all
for R in $(git remote); do git pull $R main; done
```

<!-- copybreak off -->
### Read Editions

Assuming the commit history of editions has been *ungarbled* and has valid signatures,
the working tree of the main branch will contain the contents of each edition.
An edition is either a file or directory named `object`.

```bash
find -name object
```


Testing Operations
------------------

### Check that No Editions Have Been Deleted

```bash
for H in $(git rev-list main); do
    git checkout $H
    find -name object
done | sort | uniq > all_objects.txt
git checkout main
diff all_objects.txt <(find -name object) && echo PASS || echo FAIL
```

### Check that No Editions Modified 

An *ungarbled* document succession will have only one revision per edition.
To verify that each edition has only one revision, run:

```bash
for P in $(find -name object); do git rev-list HEAD -- $P | wc -l; done
```

### Verify Signed Succession

To verify that all commits have valid signatures, run:

```bash
for H in $(git rev-list --min-parents=1 main); do
    git checkout $H~1
    git -c gpg.ssh.allowedSignersFile=signed_succession/allowed_signers verify-commit $H
done
git -c gpg.ssh.allowedSignersFile=signed_succession/allowed_signers verify-commit HEAD
git checkout main
```

Visually inspect that all the signatures are reported as good.
