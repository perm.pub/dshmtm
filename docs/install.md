# Installation

The *Document Succession Highly Manual Toolkit* consists of the following command-line tools:

* basenc
* bash
* curl
* diff
* find
* git
* jq
* sort
* ssh-keygen
* tee
* tr
* uniq
* wc
* xargs
* xxd

These tools are available on popular Linux distributions and POSIX systems, such as macOS.

## Fedora 39

On Fedora, the following `dnf` command will install all these tools:
```
dnf install bash coreutils curl diffutils findutils git jq openssh xxd
```
