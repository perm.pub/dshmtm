# Document Succession Highly Manual Toolkit Manual
<!-- copybreak off -->

The *Document Succession Highly Manual Toolkit Manual* provides instructions for
reading, writing, and testing document successions as described in the [Document
Succession Identifiers](https://perm.pub/1wFGhvmv8XZfPx0O5Hya2e9AyXo/) (DSI) and
[Document Succession Git Layout](https://perm.pub/VGajCjaNP1Ugz58Khn1JWOEdMZ8) (DSGL)
specifications.
Managing *document successions* using this toolkit is **highly manual**.
Most users will prefer using the [Hidos](https://hidos.readthedocs.io) software package instead.
This manual is for advanced users and serves as a reference implementation for
developers of software that interoperates with [Hidos](https://hidos.readthedocs.io).

The *Document Succession Highly Manual Toolkit* (DSHMT) consists of common tools available on
popular Linux distributions and POSIX systems such as macOS.
The two key packages are Git (2.34 or later) and OpenSSH (8.8 or later).
For information on which tools and packages to install,
refer to the [Installation page](install.md).
