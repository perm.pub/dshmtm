About
=====

Feel free to email feedback or questions to [Castedo Ellerman](https://castedo.com).

Technical Details
-----------------

This website is hosted by
[GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/)
as static webpages built using
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).
The domain name is hosted on [Amazon Route 53](https://aws.amazon.com/route53/).
